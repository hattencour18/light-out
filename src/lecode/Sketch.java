package lecode;

import java.util.Random; // Importer une fonction random
import processing.core.PApplet; //Ce fais automatiquement 


public class Sketch extends PApplet{ 
    
    int DIMCASE=120, NBCASES=4, COTE ;  // a redefinir selon dimensions souhaitees
    int nombrecoup = 0; //initialise le nombre de cout a 0
    int colClick ,ligClick; // coordonnees logiques de la case ou on clique
    
    Random rand = new Random(); //Aléatoire 
    boolean[][]       etat ;  // tableau representant l etat allume ou eteint des lampes
    
    boolean           redim=false;  // initialise le boolean redim a faux
    long temps; // Initialise le temps
    
    @Override
    public void setup() { //Le setup
       
        
        
        background(220);            // nuance de gris pour le fond
        
        COTE=DIMCASE*NBCASES;  // Cote égale dimension de la case x nombre de casse
        
        frame.setTitle("LightOut");  // titre de la barre de titre de la fenetre 
     
        frame.setResizable(true); //Possibilité de redim la fenêtre avec sa sourie 
      
        size(COTE+500,COTE);            // largeur et hauteur physiques de la fenetre
       
        smooth();                   // anti- crenelage
        
        
        initEtat();                         //Initialise l'état tout allumé
        
        quadrillage();              // appel de la fonction quadrillage() 
                                    //definie plus bas 
        prepa();                    //imite 50 cliques 
        
        
        for (int x=0;x<NBCASES;x++) for (int y=0;y<NBCASES;y++) dessinerLampe(x,y);
        //pour toute les cases desinner une lampes 
        temps=System.currentTimeMillis(); //importer le temps 
   
    }
    
    @Override
    public void mousePressed() {        // gestion souris
       
        colClick=mouseX/DIMCASE;  
        ligClick=mouseY/DIMCASE;
        
        changerEtat(colClick,ligClick);
        nbCoup();
        
                System.out.println(" vous avez fait " +nombrecoup+ "coups");
    }
    
    
    
    @Override
   public void keyPressed(){
    
         
            if(key == 'r'){
             
         setup();
        }
    } 
   
    
   void changerEtat( int col, int lig){   
       
       etat[col][lig]=!etat[col][lig];dessinerLampe(col,lig);
       if(col>0) { etat[col-1][lig]=!etat[col-1][lig]; dessinerLampe(col-1,lig);}
       if(lig>0) { etat[col][lig-1]=!etat[col][lig-1]; dessinerLampe(col,lig-1);}
       if(col<NBCASES-1) { etat[col+1][lig]=!etat[col+1][lig]; dessinerLampe(col+1,lig);}
       if(lig<NBCASES-1) { etat[col][lig+1]=!etat[col][lig+1]; dessinerLampe(col,lig+1);}
   }
   
   void dessinerLampe(int col, int lig){
   
      noStroke(); 
      int opacite=0; 
      if (etat[col][lig] ) opacite=90; else opacite=30;
      fill(220);
      ellipse( col*DIMCASE+DIMCASE/2,lig*DIMCASE+DIMCASE/2,DIMCASE-4,DIMCASE-4);
      fill(255,0,0,opacite);
      ellipse( col*DIMCASE+DIMCASE/2,lig*DIMCASE+DIMCASE/2,DIMCASE-4,DIMCASE-4);
  
   }
   
   
   @Override
   public void draw() {
       
     
        
  stroke(180);
     
 stroke(180);
 //change couleur
 fill(220);text("                                                ",500,50);
        fill(100,110,300);
        
     fill(255,0,0);
        rect(COTE+10,10,480,COTE-20);
       //affiche le temp//
String time="time :" +Long.toString( (System.currentTimeMillis()-temps)/1000)+" sec";

     if(redim){setup();redim=false;}    
     fill(0,0,0);
     text(time,500,50);
     
     //nombre de coup//
        text("nombre de coup "+nombrecoup,500,100);
     
   }
    

   void quadrillage() {
        
        stroke(180);      // couleur ou nuance de gris des traits et contours de figures
        strokeWeight(1);  // epaisseur des trait et contours de figures
        
        for(int nc=0;nc<=NBCASES;nc++){ // lignes verticales
            
            line(nc*DIMCASE,0,nc*DIMCASE,COTE);
        
        }  
        for(int nl=0;nl<=NBCASES;nl++){ //lignes horizontales 
        
            line(0,nl*DIMCASE,COTE,nl*DIMCASE);
        }  
           System.out.println(" vous avez fait " +nombrecoup+ "coups");
   }

    void initEtat() {   
       
       // On inititalise toutes les cases du tableau a true 
       
       etat   = new boolean[NBCASES][NBCASES];
        
       for (int x=0;x<NBCASES;x++) for (int y=0;y<NBCASES;y++) etat[x][y]=true;
        
        
    }
    
    void nbCoup(){
        
        nombrecoup++;
                                                   
    }
    
    void prepa(){
        for(int i=0;i<=rand.nextInt(50);i++){
            int x = rand.nextInt(NBCASES);
            int y = rand.nextInt(NBCASES);
            
            changerEtat(x,y);
        }
    }
    
    
    void fin() {
            
              for(int i=0;i<=rand.nextInt(50);
                      
             
           
              }
   
}
}


